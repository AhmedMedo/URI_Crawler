<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
class HomeController extends Controller
{




    public function start_craweling(Request $request)
    {
		 $main_url=trim($request->link);
		 $str = file_get_contents($main_url);
		 $base_url=parse_url($main_url, PHP_URL_HOST);
		$protocol = (strpos("'".$main_url."'",'https')) ? 'https://' : 'http://';
	
		 // Gets Webpage Title
		 if(strlen($str)>0)
		 {
		  $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
		  preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
		  $title=$title[1];
		 }
			
		 // Gets Webpage Description
		 $b =$main_url;
		 @$url = parse_url( $b );
		 @$tags = get_meta_tags($url['scheme'].'://'.$url['host'] );
		 if(array_key_exists('description', $tags))
		 {
		 	$description=$tags['description'];
		 }else
		 {
		 	$description="";
		 }

		 
			
		 // Gets Webpage Internal Links
		 $doc = new \DOMDocument; 
		 @$doc->loadHTML($str); 
		 $site_id = DB::table("website_info")->insertGetId([
		 	'title' =>$title,
		 	'description' => $description,
		 	'link'        => $protocol.$base_url,
		 	'created_at' => Carbon::now()->toDateTimeString()
		 ]);
		 @$items = $doc->getElementsByTagName('a'); 
		 foreach($items as $value) 
		 { 
		  $attrs = $value->attributes; 
		  $sec_url[]=$attrs->getNamedItem('href')->nodeValue;
		  		if($attrs->getNamedItem('href')->nodeValue !="#")
		  		{
					 DB::table("webpage_details")->insert([
					 	'site_id' =>$site_id,
					 	'internal_link' => $attrs->getNamedItem('href')->nodeValue,
					 	"full_link"     => $protocol.$base_url.$attrs->getNamedItem('href')->nodeValue

					 ]);		  			
		  		}

		 }
		 // DB::table("webpage_details")->insert([
		 // 	'link'	=>$main_url,
		 // 	'title' =>$title,
		 // 	'description' => $description,
		 // 	'internal_link' => $all_links

		 // ]);

		 return redirect('crawel/'.$site_id);

    }

    public function crawel($id)
    {
    	$links = DB::table("webpage_details")->where("site_id",$id)->get();
    	$site_base_url = DB::table("website_info")->where("id",$id)->first()->link;
		 return view('crawel',compact('links','site_base_url'));

    }
    public function old_craweling()
    {
    	$sites = DB::table("website_info")->get();
    	return view('old_craweling',compact('sites'));
    }
}
