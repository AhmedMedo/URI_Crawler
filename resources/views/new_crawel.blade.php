@extends('layouts.app')
@section("header")
<h1>Crawel
        <small>Start Your Craweling</small>
      </h1>
@endsection
@push('styles')


@endpush
@section("content")
        <form method="POST" action="{{route('start_craweling')}}" aria-label="{{ __('add_patient') }}">
                @csrf

           <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-plus"></i>  Craweling</h3>
                 <div class="box-tools pull-right">
                     <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                     </button>
                </div> 

                </div>
               
                <div class="box-body">
                  <div class="row">
          
                      <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputName">Enter Site URL Here</label>
                            <input type="text" name="link" class="form-control" id="exampleInputName" placeholder="Enter Site URL">
                          </div>
                      </div>

                      
                </div>
              </div>
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary btn-lg pull-right" id="submit">Submit</button>
              </div>
            </div>
      </form>

@endsection

