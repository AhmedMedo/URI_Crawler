@extends('layouts.app')
@section("header")
<h1>Crawel
        <small>List all Past</small>
      </h1>
@endsection 
@push('styles')


@endpush
@section("content")
        <div class="row">
            <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View All Craweling</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>id</th>
                            <th>title</th>
                            <th>links</th>
                            <th>created at</th>

                          </tr>
                          </thead>
                          <tbody>
                            @if(isset($sites))
                            @foreach($sites as $site)
                            <tr>
                              <td>{{$site->id}}</td>
                              <td>{{$site->title}}</td>
                              <td>
                                 <a href="{{route('view_crawel',$site->id)}}" class="btn btn-sm btn-info">
                                  <i class="fa fa-eye"></i> show links
                                </a>                               
                              </td>
                              <td>
                                {{$site->created_at}}
                              </td>

                            </tr>
                            @endforeach
                            @endif
                          </tbody>
                        </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

       
@endsection

@push('scripts')

 <script type="text/javascript">
       var Dtable =  $('#example1').DataTable({
              "order": [[ 3, "desc" ]]
        });
</script>
@endpush