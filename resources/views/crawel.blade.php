@extends('layouts.app')
@section("header")
<h1>Crawel
        <small>List all craweling done</small>
      </h1>
@endsection
@push('styles')


@endpush
@section("content")
        <div class="row">
            <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View All links of  <a href="{{$site_base_url}}">{{$site_base_url}}</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>id</th>
                            <th>Base Url</th>
                            <th>Parsed link</th>
                            <th>Full url</th>


                          </tr>
                          </thead>
                          <tbody>
                            @if(isset($links))
                            @foreach($links as $link)
                            <tr>
                              <td>{{$link->id}}</td>
                              <td>{{$site_base_url}}</td>
                              <td>{{$link->internal_link}}</td>
                              <td>
                                  

                                <a href="{{$link->full_link}}">{{$link->full_link}}</a>
                              </td>

                            </tr>
                            @endforeach
                            @endif
                          </tbody>
                        </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
@endsection

@push('scripts')

 <script type="text/javascript">
       var Dtable =  $('#example1').DataTable();
 </script>
@endpush