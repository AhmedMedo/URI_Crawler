<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('new_crawel');
})->name("home");
Route::post('/start_craweling','HomeController@start_craweling')->name("start_craweling");
Route::get('/crawel/{id}','HomeController@crawel')->name('view_crawel');
Route::get("old_craweling","HomeController@old_craweling")->name('old_craweling');

Route::get('/test', function () {
    $url = 'http://example.com/test';
    //get base url from link
    $base_url=parse_url($url, PHP_URL_HOST);
    //get path from link
    $path=parse_url($url, PHP_URL_PATH);
    //print base_url
    echo 'Base URL = '.$base_url;
    //print path
    echo ' PATH = '.$path;

});

